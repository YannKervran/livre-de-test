# Nom du fichier de couverture
TITRE      := $(shell grep "^title:" src/informations.yaml | sed -s 's@title: \([^\#]*\).*@\1@' | sed -r 's/ /_/g'| sed -r 's/^(.*)_/\1/g' | tr '[:upper:]' '[:lower:]')
# Collection de l’ouvrage
COLLECTION      := $(shell grep "^collection:" src/informations.yaml | sed -s 's@collection: \([^\#]*\).*@\1@' | sed -r 's/ /_/g'| sed -r 's/^(.*)_/\1/g' | tr '[:upper:]' '[:lower:]')

# Binaires pour conversion de la couverture depuis le .svg vers jpg
EXPORT := inkscape --export-filename=src/temporary_cover.png --export-area-page --export-type=png
CONVERT := convert

init: all

all : cover_prepare


cover_prepare:
ifeq (,$(wildcard src/$(TITRE).svg))
	@ wget https://framagit.org/framabook/Graphismes/-/raw/main/gabarits_couvertures/$(COLLECTION).svg --output-document=src/$(TITRE).svg
else
	@ echo "FATAL : src/$(TITRE).svg already exists, delete it first if you want to get a blank template"
endif

cover_ready:
ifeq (,$(wildcard src/cover.jpg))
ifneq (,$(wildcard src/$(TITRE).svg))
	@ $(EXPORT) src/$(TITRE).svg
	@ $(CONVERT) src/temporary_cover.png src/cover.jpg
	@ rm -Rf src/temporary_cover.png
else
	@ echo "FATAL : no src/$(TITRE).svg found, please initiate one with 'make cover_prepare' and modify it to your needs"
endif
else
	@ echo "FATAL : src/cover.jpg already exists, delete it first if you want to update from svg"
endif

